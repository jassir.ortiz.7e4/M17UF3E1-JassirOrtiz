using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class PlayerLevelController : MonoBehaviour
{
    [SerializeField] Image panelPlayerWin; 
    private int coinsCollected;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Lava")) {
            SceneManager.LoadScene("Introduccion3D");
        } else if (other.gameObject.CompareTag("Coin"))
        {
            coinsCollected += 1;
            Destroy(other.gameObject);
            if (coinsCollected >= 10)
            {
                SceneManager.LoadScene("Introduccion3D");
            }
        }
    }
}
